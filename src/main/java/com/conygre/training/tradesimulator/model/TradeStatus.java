package com.conygre.training.tradesimulator.model;

public enum TradeStatus {
    CREATED("CREATED"), PENDING("PENDING"), 
    CANCELLED("CANCELLED"), REJECTED("REJECTED"),
    FILLED("FILLED"), PARTIALLY_FILLED("PARTIALLY_FILLED"), 
    ERROR("ERROR");

    private String status;

    private TradeStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    } 
}
